<?php

use Illuminate\Database\Seeder;

class ToDoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       factory(App\User::class,5)->create()->each(function ($user){
          $user->todos()->createMany(
              factory(App\ToDo::class,8)->make()->toArray()
          );
       });


    }
}
