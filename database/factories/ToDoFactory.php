<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ToDo;
use Faker\Generator as Faker;

$factory->define(ToDo::class, function (Faker $faker) {
    return [
        'content'=>$faker->sentence($nbWords=8),
        'priority'=>$faker->numberBetween($min=0,$max=9),
    ];
});
