<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{
    protected $fillable=[
        'content',
        'priority',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
